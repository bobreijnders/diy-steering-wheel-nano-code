#ifndef __SHSTICK_H__
#define __SHSTICK_H__

#include <Arduino.h>

typedef void(*SHStickChanged) (int, byte);

#define BIT_UP 0
#define BIT_DOWN 1
#define BIT_LEFT 2
#define BIT_RIGHT 3

class SHStick {

private:
  byte xPin;
  byte yPin;
  
  int xAxisValue;
  int yAxisValue;
  int axisDeadZone; 

  byte buttonUpIndex;
  byte buttonDownIndex;
  byte buttonLeftIndex;
  byte buttonRightIndex;

  byte buttonState;
  byte buttonLastState;
  
  unsigned long buttonLastStateChanged;
  SHStickChanged shStickChangedCallback;
  
public:

  void begin(byte xAxisPin, byte yAxisPin, SHStickChanged changedcallback, byte upButton, byte downButton, byte leftButton, byte rightButton) {
    // configure PINS  X and Y axis
    xPin = xAxisPin;
    yPin = yAxisPin;

    // dead zone
    axisDeadZone = 128;

    // choose the same callback as per regular SHButtons "buttonStatusChanged" 
    shStickChangedCallback = changedcallback;

    // button mapping
    buttonUpIndex = upButton;
    buttonDownIndex = downButton;
    buttonLeftIndex = leftButton;
    buttonRightIndex = rightButton;

    // debounce
    buttonLastState = 0;
  }

  void read() {
    // determine X and Y axis
    xAxisValue = analogRead(xPin);
    yAxisValue = analogRead(yPin);

    // map to digital
    buttonState = 0;
    if(yAxisValue > 512 + (axisDeadZone*0.6)) bitSet(buttonState, BIT_UP); 
    if(yAxisValue < 512 - axisDeadZone) bitSet(buttonState, BIT_DOWN);
    if(xAxisValue < 512 - axisDeadZone) bitSet(buttonState, BIT_LEFT);
    if(xAxisValue > 512 + axisDeadZone) bitSet(buttonState, BIT_RIGHT);
    
    // callback on change
    if(buttonState != buttonLastState && buttonLastStateChanged - millis() > 100) {
      // Y
      shStickChangedCallback(buttonDownIndex, bitRead(buttonState, BIT_UP));
      shStickChangedCallback(buttonUpIndex, bitRead(buttonState, BIT_DOWN));
      // X
      shStickChangedCallback(buttonLeftIndex, bitRead(buttonState, BIT_LEFT));
      shStickChangedCallback(buttonRightIndex, bitRead(buttonState, BIT_RIGHT));

      //debounce
      buttonLastStateChanged = millis();
      buttonLastState = buttonState;
    }
  }
};

#endif
